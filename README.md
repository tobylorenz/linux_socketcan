# Introduction

This is a library to support SocketCAN from Linux.

# Build on Linux (e.g. Debian Testing)

Building under Linux works as usual:

    mkdir build
    cd build
    cmake ..
    make
    make install DESTDIR=..
    make package

# Test

Static tests are

* Cppcheck (if OPTION_RUN_CPPCHECK is set)
* CCCC (if OPTION_RUN_CCCC is set)

Dynamic tests are

* Unit tests (if OPTION_RUN_TESTS is set)
* Example runs (if OPTION_RUN_EXAMPLES is set)
* Coverage (if OPTION_USE_LCOV_GCOV is set)

The test execution can be triggered using

    make test

# Package

The package generation can be triggered using

    make package

# Repository Structure

The following files are part of the source code distribution:

* src/_project_/
* src/_project_/tests/

The following files are working directories for building and testing:

* build/_project_/

The following files are products of installation and building:

* bin/
* lib/
* share/doc/_project_/
* share/man/
* include/_project_/

# Wanted features

* Nothing is implemented yet...

# Missing test coverage

* No testing done yet.

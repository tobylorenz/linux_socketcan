cmake_minimum_required(VERSION 2.8)

include(GenerateExportHeader)

configure_file(
  config.h.in
  config.h)

find_package(Boost 1.55 COMPONENTS signals2)

include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
  ${CMAKE_CURRENT_BINARY_DIR}/../../../../../ISO_11898/include
  ${Boost_INCLUDE_DIRS})

set(source_files
  Controller.cpp
  Driver.cpp)

set(header_files
  Controller.h
  Driver.h)

add_compiler_export_flags()

add_library(${PROJECT_NAME} SHARED ${source_files} ${header_files})

if(WIN32)
  target_link_libraries(${PROJECT_NAME}
    ${Boost_LIBRARIES}
    ${PROJECT_SOURCE_DIR}/../ISO_11898/lib/ISO_11898.lib)
endif(WIN32)
if(UNIX)
  target_link_libraries(${PROJECT_NAME}
    ${Boost_LIBRARIES}
    ${PROJECT_SOURCE_DIR}/../ISO_11898/lib/libISO_11898.so)
endif(UNIX)

if(OPTION_USE_GCOV_LCOV)
  target_link_libraries(${PROJECT_NAME} gcov)
  add_definitions(-g -O0 -fprofile-arcs -ftest-coverage)
endif()

generate_export_header(${PROJECT_NAME})

if(CMAKE_COMPILER_IS_GNUCXX)
  add_definitions(-std=c++11)
endif()

install(
  TARGETS ${PROJECT_NAME}
  DESTINATION lib)

install(
  FILES
    ${CMAKE_CURRENT_BINARY_DIR}/linux_socketcan_export.h
    ${CMAKE_CURRENT_BINARY_DIR}/config.h
    ${header_files}
  DESTINATION include/Linux/SocketCAN)

#add_subdirectory(docs)

#add_subdirectory(tests)

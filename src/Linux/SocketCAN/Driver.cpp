#include "Driver.h"

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>

namespace Linux {
namespace SocketCAN {

Driver::Driver() :
    controllers()
{
}

Driver::~Driver()
{
    controllers.clear();
}

void Driver::probe()
{
#if 0
    /* register */
    for (auto uriName : uriNames) {
        Controller & controller = controllers[uriName];
        controller.init(uriName);
    }
#endif
}

}
}

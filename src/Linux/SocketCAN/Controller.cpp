#include "Controller.h"

#include <net/if.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <linux/can.h>
#include <linux/can/raw.h>
#include <iostream>

namespace Linux {
namespace SocketCAN {

class Controller::Pimpl {
public:
    Pimpl(Controller * pub);

    /** public */
    Controller * pub;

    /** URI name */
    std::string uriName;
};

Controller::Pimpl::Pimpl(Controller * pub) :
    pub(pub),
    uriName()
{
}

Controller::Controller() :
    ISO11898::Controller(),
    pimpl(new Pimpl(this))
{
}

Controller::Controller(const Controller & rhs) :
    ISO11898::Controller(),
    pimpl(new Pimpl(this))
{
    operator=(rhs);
}

void Controller::init(std::string uriName)
{
    /* uriName */
    pimpl->uriName = uriName;
    std::cerr << "construct " << pimpl->uriName << std::endl;

    /* configuration */
}

Controller::~Controller()
{
}

Controller & Controller::operator =(const Controller & rhs)
{
    if (this != &rhs) {
        configuration = rhs.configuration;
        onlineState = rhs.onlineState;
        errorState = rhs.errorState;
        statistics = rhs.statistics;
        // @todo
    }

    return *this;
}

void Controller::setOnlineState(OnlineState newOnlineState)
{
    /* prevent double connects/disconnects */
    if (newOnlineState == onlineState)
        return;

    if (newOnlineState == OnlineState::Online) {
        /* can configuration */

        /* open controller */

        onlineState = OnlineState::Online;
        onlineStateChanged(onlineState);
    } else {
        // no listen mode, so listen and offline mode are both handled as offline


        onlineState = OnlineState::Offline;
        onlineStateChanged(onlineState);
    }
}

void Controller::sendMessage(ISO11898::Message & message)
{
}

#if 0
    ISO11898::Message message;

    message.identifier = msg->data.rxMessage.frameID;
    message.remoteTransmissionRequest = (msg->data.rxMessage.flags & OCI_CAN_MSG_FLAG_REMOTE_FRAME) ? true : false;
    message.identifierExtension = (msg->data.rxMessage.flags & OCI_CAN_MSG_FLAG_EXTENDED) ? true : false;
#if BOA_VERSION_DIGIT1 >= 2
    message.extendedDataLength = (msg->data.rxMessage.flags & OCI_CAN_MSG_FLAG_FD_DATA) ? true : false;
    message.bitRateSwitch = (msg->data.rxMessage.flags & OCI_CAN_MSG_FLAG_FD_DATA_BIT_RATE) ? true : false;
    message.errorStateIndicator = (msg->data.rxMessage.flags & OCI_CAN_MSG_FLAG_FD_ERROR_PASSIVE) ? true : false;
#endif
    message.dataLengthCode = msg->data.rxMessage.dlc;
    message.data.assign(message.dataLength(), reinterpret_cast<unsigned char>(msg->data.rxMessage.data));
    pub->messageReceived(message);

    message.identifier = msg->data.canFDRxMessage.frameID;
    message.remoteTransmissionRequest = (msg->data.canFDRxMessage.flags & OCI_CAN_MSG_FLAG_REMOTE_FRAME) ? true : false;
    message.identifierExtension = (msg->data.canFDRxMessage.flags & OCI_CAN_MSG_FLAG_EXTENDED) ? true : false;
    message.extendedDataLength = (msg->data.canFDRxMessage.flags & OCI_CAN_MSG_FLAG_FD_DATA) ? true : false;
    message.bitRateSwitch = (msg->data.canFDRxMessage.flags & OCI_CAN_MSG_FLAG_FD_DATA_BIT_RATE) ? true : false;
    message.errorStateIndicator = (msg->data.canFDRxMessage.flags & OCI_CAN_MSG_FLAG_FD_ERROR_PASSIVE) ? true : false;
    message.setDataLength(msg->data.canFDRxMessage.size);
    message.data.assign(message.dataLength(), reinterpret_cast<unsigned char>(msg->data.canFDRxMessage.data));
    pub->messageReceived(message);

#endif

}
}

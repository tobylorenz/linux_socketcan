#pragma once

#include "ISO/11898.h"
#include <memory>

#include "linux_socketcan_export.h"

namespace Linux {
namespace SocketCAN {

class LINUX_SOCKETCAN_EXPORT Controller : public ISO11898::Controller {
public:
    Controller();
    Controller(const Controller & rhs);
    virtual ~Controller();
    Controller & operator =(const Controller & rhs);

    /** init */
    virtual void init(std::string uriName);

    /** set online state */
    virtual void setOnlineState(OnlineState newOnlineState);

    /** send message */
    virtual void sendMessage(ISO11898::Message & message);

private:
    class Pimpl;
    std::shared_ptr<Pimpl> pimpl;
};

}
}

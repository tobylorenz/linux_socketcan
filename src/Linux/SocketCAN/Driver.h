#pragma once

#include "linux_socketcan_export.h"

#include <map>
#include <string>

#include "Controller.h"

namespace Linux {
namespace SocketCAN {

class LINUX_SOCKETCAN_EXPORT Driver
{
public:
    Driver();
    virtual ~Driver();

    /** start searching controllers */
    void probe();

    /** map of URI names to controllers */
    std::map<std::string, Controller> controllers;
};

}
}
